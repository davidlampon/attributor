//
//  TextStatsViewController.h
//  Attributor
//
//  Created by David Lampon Diestre on 08/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextStatsViewController : UIViewController

@property (nonatomic, strong) NSAttributedString *textToAnalyze;

@end
