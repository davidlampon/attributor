//
//  AttributorAppDelegate.h
//  Attributor
//
//  Created by David Lampon Diestre on 07/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttributorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
