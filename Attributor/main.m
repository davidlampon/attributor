//
//  main.m
//  Attributor
//
//  Created by David Lampon Diestre on 07/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AttributorAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AttributorAppDelegate class]));
    }
}
